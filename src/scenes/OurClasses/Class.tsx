


type Props = {
  name:string,
  description: string,
  image:string
}

const Class = ({ name, description, image }: Props) => {
  const overlayStyles = `p-5 absolute ovelay z-50 flex h-[380px] w-[450px] flex-col
  items-center justify-center whitespace-normal bg-primary-500 text-center text-white transition
  opacity-0 duration-500 hover:opacity-70 
  `
  return (
    <li className="relative mx-5 inline-block h-[380px] w-[450px]">
      <div className={overlayStyles}>
        <p className="font-bold mb-3 text-2xl">{name}</p>
        <p className="mt-5">{description}</p>
       
      </div>
       <img alt={`${image}`} src={image} ></img>
   </li>
  )
}

export default Class